const express = require('express');
const fileDb = require('../fileDb');
const router = express.Router();

router.get('/', (req, res) => {
  res.send(fileDb.getItems());
});

router.get('/:id', (req, res) => {
  res.send('A single message by id will be here');
});

router.post('/', (req, res) => {
  const date = new Date().toISOString();
  const message = {
    message: req.body.message,
    date: date
  };
  fileDb.addItem(req.body);
  res.send({message: 'OK'});
});

module.exports = router;